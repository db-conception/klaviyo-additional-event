<?php
// create custom plugin settings menu
add_action('admin_menu', 'kae_create_menu');
$kae_textdomain = "klaviyo-additional-event";

function kae_create_menu() {

	//create new top-level menu
	add_menu_page('Klaviyo Addiotionals Events', 'Klaviyo Addiotionals Events',
'administrator', __FILE__, 'kae_settings_page' , 
plugins_url('/images/icon.png', __FILE__) );

	//call register settings function
	add_action( 'admin_init', 'register_kae_settings' );
}


function register_kae_settings() {
	//register our settings
	register_setting( 'kae-settings-group', 
'kae_ga_event_script', array(
        'description' => __('For exemple : "ga(\'send\', \'event\', { eventCategory: \'newsletter\', eventAction: \'submit\', eventLabel: \'newsletter-sidebar\'}"', "klaviyo-additional-event")
        ) );
	register_setting( 'kae-settings-group', 
'kae_ga_event_script_popup_opened' );
	register_setting( 'kae-settings-group', 'kae_ga_event_script_popup_closed' 
); 
}

function kae_settings_page() {
?>
<div class="wrap">
<h1>Klaviyo Additionals Events</h1>

<form method="post" action="options.php">
    <?php settings_fields( 'kae-settings-group' ); ?>
    <?php do_settings_sections( 'kae-settings-group' ); ?>
    <h2><?= __("additional Google Analytics events on subscribe form submit","klaviyo-additional-event")?></h2>
    <p>
        <?= __("Scripts looklikes to  : ", "klaviyo-additional-event" ) ?><code><?= 'ga(\'send\', \'event\', { eventCategory: \'newsletter\', eventAction: \'submit\', eventLabel: \'newsletter-sidebar\'});' ?></code>
    </p>
    <table class="form-table">
        <tr valign="top">
        <th scope="row">Script form submit<br></th>
        <td><input type="text" name="kae_ga_event_script" value="<?php echo 
esc_attr( get_option('kae_ga_event_script') ); ?>" /></td>
        </tr>
         
        <tr valign="top">
        <th scope="row">Script popup opened</th>
        <td><input type="text" name="kae_ga_event_script_popup_opened" value="<?php 
echo esc_attr( get_option('kae_ga_event_script_popup_opened') ); ?>" /></td>
        </tr>
        
        <tr valign="top">
        <th scope="row">Script popup closed.</th>
        <td><input type="text" name="kae_ga_event_script_popup_closed" value="<?php echo 
esc_attr( get_option('kae_ga_event_script_popup_closed') ); ?>" /></td>
        </tr>
    </table>
    
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>
