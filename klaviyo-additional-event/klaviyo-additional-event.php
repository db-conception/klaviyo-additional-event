<?php
/*
   Plugin Name: Klaviyo additional event
   Plugin URI: http://wordpress.org/extend/plugins/klaviyo-additional-event/
   Version: 0.5
   Author: db-conception
   Description: This plugin add the view content event on product page for klaviyo sync.
   Text Domain: klaviyo-additional-event
   License: GPLv3
  */

include_once("kae-options.php");


/*
 *
 * VIEW PRODUCT
 *
 */
add_action("woocommerce_after_single_product", "kae_view_product_event_print");
function kae_view_product_event_print(){

    global $product;
    $ImageUrl = wp_get_attachment_image_src( get_post_thumbnail_id( $product->id ), 'single-post-thumbnail' )[0];
    $ItemId = $product->get_id();
    $Title = $product-> get_title();
    $ProductUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $Price = $product->get_price();
    $RegularPrice = $product->get_regular_price();
    if(null === $RegularPrice)
        $RegularPrice = $Price;
    $DiscountAmount = $RegularPrice - $Price;
    $terms = get_terms( 'product_tag' );
    ?>

    <script>
        var Title = "<?php echo $Title; ?>";
        var ItemId = "<?php echo $ItemId; ?>";
        var ImageUrl = "<?php echo $ImageUrl; ?>";
        var ProductUrl = "<?php echo $ProductUrl; ?>";
        var Price = "<?php echo $Price; ?>";
        var DiscountAmount = "<?php echo $DiscountAmount; ?>";
        var RegularPrice = "<?php echo $RegularPrice; ?>";
        var _learnq = _learnq || [];

        _learnq.push(['track', 'Viewed Product', {
            Title: Title,
            ItemId: ItemId,
            ImageUrl: ImageUrl,
            Url: ProductUrl,
            Metadata: {
                Price: Price,
                DiscountAmount: DiscountAmount,
                RegularPrice: RegularPrice
            }
        }]);
    </script>
<?php
}

/*
 *
 * GA EVENT ON FORM SUBMIT
 *
 */
add_action ("wp_footer", "kae_additional_ga_event_form_submit", 80);
function kae_additional_ga_event_form_submit(){
    ?>
    <script>
        window.addEventListener("klaviyoForms", function(e) {
            <?php if (!empty( get_option('kae_ga_event_script_popup_opened') ) ): ?>
            if (e.detail.type == 'open' || e.detail.type == 'embedOpen') {
                <?= get_option('kae_ga_event_script_popup_opened') ?>
            }
            <?php endif; ?>
            <?php if (!empty( get_option('kae_ga_event_script') ) ): ?>
            if (e.detail.type == 'submit') {
                <?= get_option('kae_ga_event_script') ?>
            }
            <?php endif; ?>

            <?php if (!empty( get_option('kae_ga_event_script_popup_closed') ) ): ?>
            if (e.detail.type == 'close') {
                <?= get_option('kae_ga_event_script_popup_closed') ?>
            }
            <?php endif; ?>
        });
    </script>
<?php
}
